package com.example.exhibitions.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.exhibitions.viewModel.MainViewModel
import com.example.exhibitions.R
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_main.*
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(
            viewModelStore,
            viewModelFactory
        )[MainViewModel::class.java]
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        list.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = MainListAdapter()
        }
        initObservers()
    }

    private fun initObservers() {
        viewModel.exhibitsList.observe(viewLifecycleOwner, Observer {
            getListAdapter().setItems(it)
        })
    }

    private fun getListAdapter() = list.adapter as MainListAdapter
}