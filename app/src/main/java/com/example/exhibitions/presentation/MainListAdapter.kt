package com.example.exhibitions.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cr9ck.model.Exhibit
import com.example.exhibitions.R
import kotlinx.android.synthetic.main.item_list.view.*


class MainListAdapter : RecyclerView.Adapter<MainListAdapter.ViewHolder>() {

    private var itemsList = emptyList<Exhibit>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false))

    override fun getItemCount() = itemsList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemsList[position])
    }

    fun setItems(items: List<Exhibit>) {
        itemsList = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(exhibit: Exhibit) {
            itemView.apply {
                exhibit.images.forEach {
                    val imageView = AppCompatImageView(context)
                    Glide.with(context)
                        .load(it)
                        .into(imageView)

                    val imageContainer = ImageContainer(context)
                        .withImage(imageView)
                        .withTitle(exhibit.title)
                    linear.addView(imageContainer)
                }
            }
        }
    }
}