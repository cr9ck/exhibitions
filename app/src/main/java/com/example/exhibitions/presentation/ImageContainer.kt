package com.example.exhibitions.presentation

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import com.example.exhibitions.R


class ImageContainer @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val constraintSet = ConstraintSet()
    private val imageViewParams = LayoutParams(
        LayoutParams.WRAP_CONTENT,
        resources.getDimension(R.dimen.item_height).toInt()
    )

    private val textViewParams = LayoutParams(
        LayoutParams.WRAP_CONTENT,
        LayoutParams.WRAP_CONTENT
    )

    override fun onFinishInflate() {
        super.onFinishInflate()
        layoutParams = imageViewParams
    }

    fun withImage(imageView: AppCompatImageView): ImageContainer {
        imageView.id = View.generateViewId()
        imageView.layoutParams = imageViewParams
        addView(imageView)
        constraint(ConstraintSide.TOP, imageView)
        constraint(ConstraintSide.END, imageView)
        constraint(ConstraintSide.START, imageView)
        constraint(ConstraintSide.BOTTOM, imageView)
        return this
    }

    fun withTitle(title: String): ImageContainer {
        val titleView = TextView(context)
        titleView.id = View.generateViewId()
        titleView.text = title
        titleView.layoutParams = textViewParams
        addView(titleView)
        constraint(ConstraintSide.TOP, titleView, resources.getDimension(R.dimen.title_margin).toInt())
        constraint(ConstraintSide.END, titleView, resources.getDimension(R.dimen.title_margin).toInt())
        return this
    }

    private fun constraintToTop(view: View, margin: Int = 0) {
        constraintSet.clone(this)
        constraintSet.connect(
            view.id,
            ConstraintSet.TOP,
            ConstraintSet.PARENT_ID,
            ConstraintSet.TOP,
            margin
        )
        constraintSet.applyTo(this)
    }

    private fun constraintToBottom(view: View, margin: Int = 0) {
        constraintSet.clone(this)
        constraintSet.connect(
            view.id,
            ConstraintSet.BOTTOM,
            ConstraintSet.PARENT_ID,
            ConstraintSet.BOTTOM,
            margin
        )
        constraintSet.applyTo(this)
    }

    private fun constraintToStart(view: View, margin: Int = 0) {
        constraintSet.clone(this)
        constraintSet.connect(
            view.id,
            ConstraintSet.START,
            ConstraintSet.PARENT_ID,
            ConstraintSet.START,
            margin
        )
        constraintSet.applyTo(this)
    }

    private fun constraintToEnd(view: View, margin: Int = 0) {
        constraintSet.clone(this)
        constraintSet.connect(
            view.id,
            ConstraintSet.END,
            ConstraintSet.PARENT_ID,
            ConstraintSet.END,
            margin
        )
        constraintSet.applyTo(this)
    }

    private fun constraint(side: ConstraintSide, view: View, margin: Int = 0) {
        when (side) {
            ConstraintSide.TOP -> constraintToTop(view, margin)
            ConstraintSide.BOTTOM -> constraintToBottom(view, margin)
            ConstraintSide.START -> constraintToStart(view, margin)
            ConstraintSide.END -> constraintToEnd(view, margin)
        }
    }

    private enum class ConstraintSide {
        TOP, BOTTOM, START, END
    }
}