package com.example.exhibitions

import com.example.exhibitions.di.AppComponent
import com.example.exhibitions.di.AppComponentProvider
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class Application: DaggerApplication() {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        appComponent = AppComponentProvider.provideAppComponent(this)
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return appComponent
    }
}