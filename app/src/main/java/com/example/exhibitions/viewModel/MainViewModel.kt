package com.example.exhibitions.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cr9ck.model.Exhibit
import com.cr9ck.model.ExhibitLoader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private val exhibitLoader: ExhibitLoader): ViewModel() {

    val exhibitsList: LiveData<List<Exhibit>> by lazy { getExhibitsList() }

    private fun getExhibitsList() = MutableLiveData<List<Exhibit>>().apply {
        viewModelScope.launch(Dispatchers.IO) {
            postValue(exhibitLoader.getExhibitList())
        }
    }
}