package com.example.exhibitions.di

import android.content.Context
import com.cr9ck.fileexhibitsloader.di.DaggerLoaderComponent

object AppComponentProvider {

    fun provideAppComponent(context: Context): AppComponent {
        val loaderComponent = DaggerLoaderComponent.factory().create(context)
        return DaggerAppComponent.builder()
            .loaderComponent(loaderComponent)
            .build()
    }
}