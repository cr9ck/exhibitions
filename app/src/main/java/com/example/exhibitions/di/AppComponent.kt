package com.example.exhibitions.di

import androidx.lifecycle.ViewModelProvider
import com.example.exhibitions.Application
import com.cr9ck.fileexhibitsloader.di.LoaderComponent
import com.cr9ck.model.ExhibitLoader
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

@Component(
    dependencies = [LoaderComponent::class],
    modules = [AndroidInjectionModule::class,
        AppModule::class]
)
interface AppComponent: AndroidInjector<Application> {

    fun provideExhibitLoader(): ExhibitLoader

    fun provideViewModelFactory(): ViewModelProvider.Factory
}