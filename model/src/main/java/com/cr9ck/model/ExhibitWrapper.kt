package com.cr9ck.model

data class ExhibitWrapper(val list: List<Exhibit>)