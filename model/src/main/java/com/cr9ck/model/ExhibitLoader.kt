package com.cr9ck.model

interface ExhibitLoader {

    fun getExhibitList(): List<Exhibit>
}