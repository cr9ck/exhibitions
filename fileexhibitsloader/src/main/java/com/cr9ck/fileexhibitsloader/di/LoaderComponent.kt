package com.cr9ck.fileexhibitsloader.di

import android.content.Context
import com.cr9ck.model.ExhibitLoader
import dagger.BindsInstance
import dagger.Component

@Component(modules = [LoaderModule::class])
interface LoaderComponent {

    fun getExhibitLoader(): ExhibitLoader

    @Component.Factory
    interface LoaderFactory {
        fun create(@BindsInstance context: Context): LoaderComponent
    }
}