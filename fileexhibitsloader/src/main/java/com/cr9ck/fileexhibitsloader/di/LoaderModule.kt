package com.cr9ck.fileexhibitsloader.di

import android.content.Context
import com.cr9ck.fileexhibitsloader.ExhibitsLoaderImpl
import com.cr9ck.model.ExhibitLoader
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides

@Module
class LoaderModule {

    @Provides
    fun provideLoader(gson: Gson, context: Context): ExhibitLoader = ExhibitsLoaderImpl(gson, context)

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()
}