package com.cr9ck.fileexhibitsloader

import android.content.Context
import com.cr9ck.model.Exhibit
import com.cr9ck.model.ExhibitLoader
import com.cr9ck.model.ExhibitWrapper
import com.google.gson.Gson

class ExhibitsLoaderImpl(private val gson: Gson, private val context: Context) : ExhibitLoader {

    override fun getExhibitList(): List<Exhibit> {
        val inputStream = context.resources.assets.open("ios_challenge.json")
        val buffer = inputStream.readBytes()
        inputStream.let {
            it.read(buffer)
            it.close()
        }
        val exhibitsWrapper = gson.fromJson(String(buffer), ExhibitWrapper::class.java)
        return exhibitsWrapper.list
    }
}